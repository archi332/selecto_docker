1. Clone docker git@gitlab.com:archi332/selecto.git
2. Rename '.env.dist' => '.env' and configure it
3. Clone project git@gitlab.com:archi332/selecto.git into path, specified in '.env' - SYMFONY_APP_PATH
4. Open terminal inside folder with cloned docker  
5. Run 'docker-compose build'
6. Run 'docker-compose up -d'
7. Run 'docker-compose run -u www-data php bash'
8. Run 'composer install'
9. 'php bin/console d:m:m -e=dev'
10. 'php bin/console d:f:l'

11. POST request
http://selecto.loc/app_dev.php/api/login_check

```JSON
{
	"_username":"Admin@mail.com",
	"_password":"admin"
}
```
for admin role or
```
{
	"_username":"User@mail.com",
	"_password":"user"
}
```

for sample with user role

Will get

```JSON
{
    "token": "eyJhbGciOiJSUz..."
}
```

All next requests MUST HAVE header with token:
```
Authorization:Bearer eyJhbGciOiJSUz...

```

12. GET will show all users (if role gives access) with order and filter
http://selecto.loc/app_dev.php/api/user?sort=id&order=desc&name=00&email=1796

13. GET http://selecto.loc/app_dev.php/api/user/1
Shows specified user

14. DELETE http://selecto.loc/app_dev.php/api/user/1
Deletes user

15. PUT http://selecto.loc/app_dev.php/api/user/1
Update user

16. POST http://selecto.loc/app_dev.php/api/user
Create user



